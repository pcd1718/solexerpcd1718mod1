# Statistics regarding the first set of exercise delivery

In total I received ```105 deliveries``` of which ```~20% were malformed``` (missing directory structure or adhere to the specification),
```5 did not compile``` of which 2 were corrected with a slight intervention, ```2 had missing library definitions and system locales``` 
that did not match my environment. All were corrected, except 1 (```studentIdentifier: 1049140```) which has grave compilation errors.

As you can see from the stats., many of you did not follow the rules regarding project delivery. Some projects needed special attention 
in order to compile and 2/3 of them had an ad-hoc structure that required manual intervention in order to build.

Please, refer to the delivery file for more info. If for some reason you do not find you student identifier in the list and you strongly believe 
you have delivered the project - please drop me an email. 

# Q&A regarding the exercises

Below is a short Q&A, grouped under thematics categories, regarding this first delivery:

### 1. Should library objects be immutable?

No, the absence of mutators in SOME of the classes was a pure coincidence and a modelling choice to which You should have adhered.
We have discussed examples of immutable classes in the Java library e.g., java.lang.String and none of the classes in our example
was for instance declared final.

One of the goals in the library scenario, was to avoid side-effects from the callee by employing defensive programming techniques
such as copying parameters from mutator methods and returning a copy of the object's internal state - copy insteadof a reference
which could be exploited externally to access and modify your objects internal state without going through the instance methods.
Hence potentially violating class invariants. 

### 2. Why do some library classes implement the cloneable interface?

This was added intentionally to make you reflect on a correct implementation of a clone method. You should have overridden the Object::clone method
and provided a safe implementation whereby safe is meant that the implementation should not bind the copy and original object's state together.

### 3. I have read around that object cloning is not advisable in Java?

Indeed there are better ways to achieve the same goal i.e., the copy constructor or factory clone approach (1 of you has followed this approach). For more information regarding the subject refer to (Effective Java by Joshua Bloch - Item 11: Override clone judiciously). 

This said, you should have overridden the clone method and provided a correct implementation that did not violate class invariants and more importantly created a distinct state for the copy and original object.

### 4. Is it mandatory to use lambdas and STREAM API for this delivery?

No it is/was not. Several parts of the exercises could have been implemented (in an elegant way) using the Java 8 lang. additions. However, we care most about code correctness that syntactic sugar - we should care about complexity but this is not evaluated in this exercise. 
Also, some of the deliveries that approached the solution with Stream API/lambdas should have been careful in considering some corner cases i.e., Optinal(s) with no result should be accessed thorugh appropriate methods otherwise a RT exception is thrown.

### 5. Is it acceptable to add additional auxiliary methods, classes?

Yes. The tests evaluate only the public API of the original project. Also, some of you have added additional, more strict, controls to class construction - this is ok. The tests in this case check only for corner cases - those declared in the API. Some of you have made additional considerations regarding code complexity

### 6. Evaluation

I will not publish the individual points and all will be announced after the second set of exercises is delivered. 
With the exception of project (studentIdentifier: 1049140) that is not correct, all the others can deliver the second set of exercises in due time.

