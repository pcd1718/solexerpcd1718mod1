package statistics;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.stream.Collectors;
import java.util.Map;


/**
 * This class can be considered as a final helper class used to compute the balance sheet of a company.  
 * The program takes as arguments 2 filenames corresponding to the balance_input_file and balance_output_file,
 * respectively. 
 * An entry in the balance input file represents the monthly budget of the company for that specific year.
 * The output file format is a synthesized version of the former, reporting the mean and average balance for 
 * a particular year.
 * For more insights on the file format refer to the provided {input, output}.txt files.
 */

public final class PCD1718StatisticheAnnuali {

	/**
	 * Fills in the MultiMap data structure containing the raw data read from the input file.
	 * 
	 * @param map:			the map structure to fill with data
	 * @param inputFilePath	the path of the input balance sheet file 
	 * 
	 * @throws IOException  if the file denoted by the inputFilePath parameter does not exist
	 */
	public static void buildMapFromInput(MultiMap<Integer, Double> map, String inputFilePath) throws IOException {
		
		File inputFile = new File(inputFilePath);
		
		buildMapFromInput(map, inputFile);
	}

	/**
	 * Fills in the MultiMap data structure containing the raw data read from the input file.
	 * 
	 * @param map:			the map structure to fill with data
	 * @param inputFile		the path of the input balance sheet file 
	 * 
	 * @throws IOException  if the file denoted by the inputFilePath parameter does not exist
	 * @throws IllegalArgumentException if the input parameters are not valid
	 */
	public static void buildMapFromInput(MultiMap<Integer, Double> map, File inputFile) throws IOException {
				
		if(!inputFile.exists()) {//a FileNotFoundException is also appropriate
			throw new IllegalArgumentException("Input file does not exist");
		}

		if(map == null) {
			throw new IllegalArgumentException("Map is null");
		}
		
		int row 			= 0;
		int key 			= 0;
		double value 		= 0d;
		String[] entries 	= null;
		String line 		= null;

		try (BufferedReader inBuff = new BufferedReader(new FileReader(inputFile))) 
		{
			while( (line = inBuff.readLine()) != null) 
			{
				if(row != 0) 
				{
					entries = line.split("\t");

					if(entries.length != 12 + 1)
						System.err.println("Probably missing some data");
					
					try {							

						key = Integer.valueOf(entries[0]);

					}catch(NumberFormatException nrex) {
						throw new IOException("FileFormatNotSupported: erroneous entry in row(" + row + ") col(" + 0 + ")");
					}					
					
					for(int i=1; i<entries.length; i++) 
					{						
						try {
							value = Double.valueOf(entries[i]);

						}catch(NumberFormatException nrex) {
							throw new IOException("FileFormatNotSupported: erroneous entry in row(" + row + ") col(" + i + ")");
						}
						map.put(Integer.valueOf(key), Double.valueOf(value));					
					}
				}
				row++;				
			}
		}catch(IOException ioex) {
			throw ioex;
		}
		
	}

	/**
	 * Produces the synthesized version of the input balance file from the data stored in the multimap data structure.
	 * 
	 * @param map:			a MultiMap containing the balance data
	 * @param balanceOut:	file where the synthesized version of the balance sheet is stored
	 * 
	 * @throws IOException: if the I/O operation fails
	 * @throws IllegalArgumentException: if the input parameters are not safe
	 */
	public static void outputStatisticsFile(MultiMap<Integer, Double> map, File balanceOut) throws IOException {

		if(map == null) 
			throw new IllegalArgumentException("Map is null");
		

		Map<Integer, Double> avgs = map.keySet().stream()
	                        					.collect(Collectors.toMap( 
	                        							k->k, //keys
	                        							k -> map.get(k).stream()//entries
	                        										   .mapToDouble(Double::doubleValue)
	                        										   .average().orElse(0))
	                        						  );

		Map<Integer, Double> stdDev = map.keySet().stream()
												  .collect(Collectors.toMap( 
														  k->k, //keys
														  k -> map.get(k).stream()//entries
														  				 .mapToDouble(v->Math.pow(v.doubleValue()-avgs.get(k), 2))
														  				 .average().orElse(0))
														  );		
		
		try (BufferedWriter outBuff = new BufferedWriter(new FileWriter(balanceOut))) {//OSS.: incremental write (file too large?!)		
			outBuff.write("\t" + "Mean\tStdDev\n");
			for(Integer year: map.keySet())
				outBuff.write(year + "\t" + avgs.get(year) + "\t" + Math.sqrt(stdDev.get(year)) + "\n");
		}catch(IOException ioex) {
			throw ioex;
		}	
	}
	
		
	/**
	 * Produces a synthesized version of the input balance sheet. This method can be seen as a sequential pipeline
	 * combining the buildMapFromInput(...) method and the outputStatisticsFile(...)
	 * 
	 * @param inputFile:	denotes the balance sheet file path	
	 * @param outputFile:	denotes the file where the output balance sheet should be stored
	 * 
	 * @throws IOException: in case some I/O error occurred e.g., the file does not adhere to a specific format or an
	 * 						unexpected error during read/write.
	 * @throws IllegalArgumentException: in case input parameters are not valid
	 */
	public static void produceSynthetizedBalanceSheet(String inputFile, String outputFile) throws IOException {

		if(inputFile == null || outputFile == null) {
			throw new IllegalArgumentException("Parameters are not valid");
		}
		
		File fIn = new File(inputFile);
		File fOut = new File(outputFile);
				
		MultiMap<Integer, Double> map = new MultiMap<Integer, Double>();
		buildMapFromInput(map, fIn);
		outputStatisticsFile(map, fOut);
	}	
}
